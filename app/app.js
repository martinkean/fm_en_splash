var App = angular.module('App', []);


App.controller('BookController', function($scope, $http) {
  $scope.q = '';
  $http.get('data/book_details.json')
       .then(function(res){
            $bookspre = res.data;
          	$scope.books = makeRandom($bookspre);
        });
    //add filter http://plnkr.co/edit/Aowec7MzflOdPjPRgVHE?p=preview

  var match = function (item, val) {
    var regex = new RegExp(val, 'i');
    return item.description.match(regex) != null || 
            item.title.match(regex) != null ||
            item.category.match(regex) != null 
    ;
  };
  
  $scope.filterBooks = function(book) {
    // No filter, so return everything
    if (!$scope.q) return true;
    var matched = true;
    
    // Otherwise apply your matching logic
    $scope.q.split(' ').forEach(function(token) {
        matched = matched && match(book, token); 
    });
     
    return matched;
  };

});


//this app adapted from https://plnkr.co/edit/81oj8mHaNyfQpCmRBWO4?p=preview

// iterate in batches of 4 from #http://stackoverflow.com/questions/21644493/how-to-split-the-ng-repeat-data-with-three-columns-using-bootstrap

App.controller('BookController2', function($scope, $http) {
  $http.get('data/brief_details.json')
       .then(function(res){
          $bookspre = res.data;
          $scope.books = makeRandom($bookspre);
            });
         
});

function makeRandom(inputArray){
    angular.forEach(inputArray, function(value){
      value.rank = Math.random();
    });
    return inputArray; 
  }

//order randomly from http://jsfiddle.net/owenmead/fa4v8/1/

// but didn't work see http://stackoverflow.com/questions/21586369/random-orderby-in-angularjs-1-2-returns-infdig-errors
    